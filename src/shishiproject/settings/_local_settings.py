# ShiShi VTO server settings, they should match the ones in ShiShi Studio at studio.shishiapp.com

SHISHI_VTO_KEY = '(shishi vto server key)'

SHISHI_ACCOUNT_ID = 0


# Use AWS S3 to store graphic assets

# DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

# AWS S3 settings

# Recommend to use CDN
# AWS_S3_CUSTOM_DOMAIN = '(subdomain).cloudfront.net'

# The following settings are not needed if the bucket has public access
# AWS_ACCESS_KEY_ID = '(s3 access key)'
# AWS_SECRET_ACCESS_KEY = '(s3 secret access key)'
# AWS_STORAGE_BUCKET_NAME = '(s3 bucket name)'





# Use Azure Storage to store graphic assets

DEFAULT_FILE_STORAGE = 'storages.backends.azure_storage.AzureStorage'

# Azure Storage settings
AZURE_ACCOUNT_NAME = '(azure account name)'

AZURE_ACCOUNT_KEY = '(azure private key)'

AZURE_CONTAINER = '(azure container)'




# These are django environment settings

SECRET_KEY = '(django secret key)'

DEBUG = True

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'shishidb',
        'USER': 'shishidbuser',
        'PASSWORD': 'postgres',
        'HOST': 'db',
        'PORT': '5432',
    },
}

LANGUAGE_CODE = 'en'
