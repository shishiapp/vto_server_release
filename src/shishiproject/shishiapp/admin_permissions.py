from rest_framework import permissions
from django.conf import settings

class RestorePermission(permissions.BasePermission):
    def has_permission(self, request, view):

        if request.method == 'POST':
            key = request.META.get('HTTP_SERVER_KEY', None)

            if key == settings.SHISHI_VTO_KEY:
                return True

            return False

        return False