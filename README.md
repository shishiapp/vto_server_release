#### 1. clone docker repo

```git clone https://bitbucket.org/shishiapp/vto_server_release.git```

#### 2 get ssl for the domain, copy certificate files to

```ssl/cert.crt and ssl/cert.key```

#### 3 configure django

create local_settings.py from template \_local_settings.py

```
cd src/shishiproject/settings; cp _local_settings.py local_settings.py
```

then modify local_settings.py:

##### 3.1 SECRET_KEY

generate by running 

```
python -c 'import random; print("".join([random.choice("abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)") for i in range(50)]))'
```

##### 3.2 SHISHI_ACCOUNT_ID

find in shishi studio's account page

##### 3.3 SHISHI_VTO_KEY

set in shishi studio's account page and use it here, can be any string

##### 3.4 DEBUG

set to False in production


#### 4 now run 

```sudo docker-compose up```

docker will download some images, download django dependencies and run django migrations

when all containers are started, open https://domain:8000/admin in browser

#### 5 go to studio.shishiapp.com, log in and open account page

##### 5.1 setup

server url: https://domain:8000

server key: the same as 3.3 SHISHI_VTO_KEY

admin password: pick a password for django user 'admin'

##### 5.2 backup

make a backup of the vto data for the current account

##### 5.3 migrate

sync the data to vto server

#### 6 setup image assets

##### 6.1 copy images to the storage service, s3 or azure

##### 6.2 configure django for storage, modify respective parameters in local_settings.py















