from . import models, admin_permissions
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from django.core.management import call_command
import os
import zipfile, shutil
from django.contrib.auth.models import User
from shishiproject.shishiapp import apps

class RestoreView(APIView):
    permission_classes = (admin_permissions.RestorePermission, )

    def post(self, request):

        file = request.FILES['file']
        migration_name = request.META.get('HTTP_MIGRATION_NAME', '')
        admin_password = request.META.get('HTTP_ADMIN_PASSWORD', '')

        unzipped = zipfile.ZipFile(file)

        settings_dir = os.path.dirname(__file__)
        project_root = os.path.abspath(os.path.dirname(settings_dir))

        fixture_root = os.path.join(project_root, 'shishiapp/fixtures')

        old_fixture_list = [f for f in os.listdir(fixture_root) if not f.startswith('.')]

        unzipped.extractall(path=fixture_root)

        directory = os.path.dirname(unzipped.namelist()[0])

        # in case it's the same backup
        new_fixture = os.path.basename(os.path.normpath(directory))
        if new_fixture in old_fixture_list:
            old_fixture_list.remove(new_fixture)

        call_command('flush', interactive=False)

        call_command('loaddata', os.path.join(directory, 'categories.json'))
        call_command('loaddata', os.path.join(directory, 'styles.json'))
        call_command('loaddata', os.path.join(directory, 'style_assets.json'))
        call_command('loaddata', os.path.join(directory, 'style_has_assets.json'))
        call_command('loaddata', os.path.join(directory, 'glitters.json'))
        call_command('loaddata', os.path.join(directory, 'lenses.json'))
        call_command('loaddata', os.path.join(directory, 'shapes.json'))
        call_command('loaddata', os.path.join(directory, 'brands.json'))
        call_command('loaddata', os.path.join(directory, 'accounts.json'))
        call_command('loaddata', os.path.join(directory, 'finishes.json'))
        call_command('loaddata', os.path.join(directory, 'concepts.json'))
        call_command('loaddata', os.path.join(directory, 'product_items.json'))
        call_command('loaddata', os.path.join(directory, 'tryon_items.json'))
        call_command('loaddata', os.path.join(directory, 'tryon_groups.json'))
        call_command('loaddata', os.path.join(directory, 'looks.json'))
        call_command('loaddata', os.path.join(directory, 'tryon_group_of_looks.json'))
        call_command('loaddata', os.path.join(directory, 'tryon_group_of_look_has_items.json'))

        if admin_password != '':
            user = User.objects.create_superuser(username='admin', email='', password=admin_password)
            user.save()

        migration = models.ShiShiMigration()
        migration.name = migration_name
        migration.save()

        apps.db_timestamp = int(migration.timestamp.timestamp())


        # remove old dirs after successfully applied fixture
        for old_fixture in old_fixture_list:
            shutil.rmtree(os.path.join(fixture_root, old_fixture))

        return Response("Success", status=status.HTTP_200_OK)
