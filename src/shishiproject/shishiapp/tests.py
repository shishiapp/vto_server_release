import unittest
from django.core.urlresolvers import reverse
from django.test import Client
from .models import Brand, Concept, ProductItem, TryOnItem, Category, Finish, TryOnGroup, Style, Look, Comment
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType


def create_django_contrib_auth_models_user(**kwargs):
    defaults = {}
    defaults["username"] = "username"
    defaults["email"] = "username@tempurl.com"
    defaults.update(**kwargs)
    return User.objects.create(**defaults)


def create_django_contrib_auth_models_group(**kwargs):
    defaults = {}
    defaults["name"] = "group"
    defaults.update(**kwargs)
    return Group.objects.create(**defaults)


def create_django_contrib_contenttypes_models_contenttype(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ContentType.objects.create(**defaults)


def create_brand(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["image"] = "image"
    defaults.update(**kwargs)
    return Brand.objects.create(**defaults)


def create_concept(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["image"] = "image"
    defaults.update(**kwargs)
    if "concept_of_brand" not in defaults:
        defaults["concept_of_brand"] = create_brand()
    if "concept_of_category" not in defaults:
        defaults["concept_of_category"] = create_category()
    return Concept.objects.create(**defaults)


def create_productitem(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["image"] = "image"
    defaults["description"] = "description"
    defaults["url"] = "url"
    defaults.update(**kwargs)
    if "product_item_of_concept" not in defaults:
        defaults["product_item_of_concept"] = create_concept()
    return ProductItem.objects.create(**defaults)


def create_tryonitem(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["color_code"] = "color_code"
    defaults["color_name"] = "color_name"
    defaults["finish_name"] = "finish_name"
    defaults["coverage"] = "coverage"
    defaults.update(**kwargs)
    if "tryon_item_of_product_item" not in defaults:
        defaults["tryon_item_of_product_item"] = create_productitem()
    if "tryon_item_of_finish" not in defaults:
        defaults["tryon_item_of_finish"] = create_finish()
    return TryOnItem.objects.create(**defaults)


def create_category(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults.update(**kwargs)
    return Category.objects.create(**defaults)


def create_finish(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["real_finish"] = "real_finish"
    defaults.update(**kwargs)
    if "finish_of_category" not in defaults:
        defaults["finish_of_category"] = create_category()
    return Finish.objects.create(**defaults)


def create_tryongroup(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["items"] = "items"
    defaults["styles"] = "styles"
    defaults.update(**kwargs)
    if "tryon_group_of_product_item" not in defaults:
        defaults["tryon_group_of_product_item"] = create_productitem()
    return TryOnGroup.objects.create(**defaults)


def create_style(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["image"] = "image"
    defaults.update(**kwargs)
    if "style_of_category" not in defaults:
        defaults["style_of_category"] = create_category()
    return Style.objects.create(**defaults)


def create_look(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["detail"] = "detail"
    defaults["description"] = "description"
    defaults["image"] = "image"
    defaults.update(**kwargs)
    if "created_by" not in defaults:
        defaults["created_by"] = create_django_contrib_auth_models_user()
    if "look_has_comment" not in defaults:
        defaults["look_has_comment"] = create_django_contrib_auth_models_user()
    return Look.objects.create(**defaults)


def create_commentonlook(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["comment"] = "comment"
    defaults["stars"] = "stars"
    defaults.update(**kwargs)
    if "comment_by_user" not in defaults:
        defaults["comment_by_user"] = create_django_contrib_auth_models_user()
    if "comment_on_look" not in defaults:
        defaults["comment_on_look"] = create_look()
    return CommentOnLook.objects.create(**defaults)


class BrandViewTest(unittest.TestCase):
    '''
    Tests for Brand
    '''
    def setUp(self):
        self.client = Client()

    def test_list_brand(self):
        url = reverse('shishiapp_brand_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_brand(self):
        url = reverse('shishiapp_brand_create')
        data = {
            "name": "name",
            "image": "image",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_brand(self):
        brand = create_brand()
        url = reverse('shishiapp_brand_detail', args=[brand.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_brand(self):
        brand = create_brand()
        data = {
            "name": "name",
            "image": "image",
        }
        url = reverse('shishiapp_brand_update', args=[brand.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ConceptViewTest(unittest.TestCase):
    '''
    Tests for Concept
    '''
    def setUp(self):
        self.client = Client()

    def test_list_concept(self):
        url = reverse('shishiapp_concept_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_concept(self):
        url = reverse('shishiapp_concept_create')
        data = {
            "name": "name",
            "image": "image",
            "concept_of_brand": create_brand().pk,
            "concept_of_category": create_category().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_concept(self):
        concept = create_concept()
        url = reverse('shishiapp_concept_detail', args=[concept.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_concept(self):
        concept = create_concept()
        data = {
            "name": "name",
            "image": "image",
            "concept_of_brand": create_brand().pk,
            "concept_of_category": create_category().pk,
        }
        url = reverse('shishiapp_concept_update', args=[concept.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ProductItemViewTest(unittest.TestCase):
    '''
    Tests for ProductItem
    '''
    def setUp(self):
        self.client = Client()

    def test_list_productitem(self):
        url = reverse('shishiapp_productitem_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_productitem(self):
        url = reverse('shishiapp_productitem_create')
        data = {
            "name": "name",
            "image": "image",
            "description": "description",
            "url": "url",
            "product_item_of_concept": create_concept().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_productitem(self):
        productitem = create_productitem()
        url = reverse('shishiapp_productitem_detail', args=[productitem.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_productitem(self):
        productitem = create_productitem()
        data = {
            "name": "name",
            "image": "image",
            "description": "description",
            "url": "url",
            "product_item_of_concept": create_concept().pk,
        }
        url = reverse('shishiapp_productitem_update', args=[productitem.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TryOnItemViewTest(unittest.TestCase):
    '''
    Tests for TryOnItem
    '''
    def setUp(self):
        self.client = Client()

    def test_list_tryonitem(self):
        url = reverse('shishiapp_tryonitem_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_tryonitem(self):
        url = reverse('shishiapp_tryonitem_create')
        data = {
            "name": "name",
            "color_code": "color_code",
            "color_name": "color_name",
            "finish_name": "finish_name",
            "coverage": "coverage",
            "tryon_item_of_product_item": create_productitem().pk,
            "tryon_item_of_finish": create_finish().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_tryonitem(self):
        tryonitem = create_tryonitem()
        url = reverse('shishiapp_tryonitem_detail', args=[tryonitem.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_tryonitem(self):
        tryonitem = create_tryonitem()
        data = {
            "name": "name",
            "color_code": "color_code",
            "color_name": "color_name",
            "finish_name": "finish_name",
            "coverage": "coverage",
            "tryon_item_of_product_item": create_productitem().pk,
            "tryon_item_of_finish": create_finish().pk,
        }
        url = reverse('shishiapp_tryonitem_update', args=[tryonitem.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CategoryViewTest(unittest.TestCase):
    '''
    Tests for Category
    '''
    def setUp(self):
        self.client = Client()

    def test_list_category(self):
        url = reverse('shishiapp_category_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_category(self):
        url = reverse('shishiapp_category_create')
        data = {
            "name": "name",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_category(self):
        category = create_category()
        url = reverse('shishiapp_category_detail', args=[category.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_category(self):
        category = create_category()
        data = {
            "name": "name",
        }
        url = reverse('shishiapp_category_update', args=[category.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class FinishViewTest(unittest.TestCase):
    '''
    Tests for Finish
    '''
    def setUp(self):
        self.client = Client()

    def test_list_finish(self):
        url = reverse('shishiapp_finish_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_finish(self):
        url = reverse('shishiapp_finish_create')
        data = {
            "name": "name",
            "real_finish": "real_finish",
            "finish_of_category": create_category().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_finish(self):
        finish = create_finish()
        url = reverse('shishiapp_finish_detail', args=[finish.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_finish(self):
        finish = create_finish()
        data = {
            "name": "name",
            "real_finish": "real_finish",
            "finish_of_category": create_category().pk,
        }
        url = reverse('shishiapp_finish_update', args=[finish.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TryOnGroupViewTest(unittest.TestCase):
    '''
    Tests for TryOnGroup
    '''
    def setUp(self):
        self.client = Client()

    def test_list_tryongroup(self):
        url = reverse('shishiapp_tryongroup_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_tryongroup(self):
        url = reverse('shishiapp_tryongroup_create')
        data = {
            "name": "name",
            "items": "items",
            "styles": "styles",
            "tryon_group_of_product_item": create_productitem().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_tryongroup(self):
        tryongroup = create_tryongroup()
        url = reverse('shishiapp_tryongroup_detail', args=[tryongroup.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_tryongroup(self):
        tryongroup = create_tryongroup()
        data = {
            "name": "name",
            "items": "items",
            "styles": "styles",
            "tryon_group_of_product_item": create_productitem().pk,
        }
        url = reverse('shishiapp_tryongroup_update', args=[tryongroup.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class StyleViewTest(unittest.TestCase):
    '''
    Tests for Style
    '''
    def setUp(self):
        self.client = Client()

    def test_list_style(self):
        url = reverse('shishiapp_style_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_style(self):
        url = reverse('shishiapp_style_create')
        data = {
            "name": "name",
            "image": "image",
            "style_of_category": create_category().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_style(self):
        style = create_style()
        url = reverse('shishiapp_style_detail', args=[style.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_style(self):
        style = create_style()
        data = {
            "name": "name",
            "image": "image",
            "style_of_category": create_category().pk,
        }
        url = reverse('shishiapp_style_update', args=[style.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class LookViewTest(unittest.TestCase):
    '''
    Tests for Look
    '''
    def setUp(self):
        self.client = Client()

    def test_list_look(self):
        url = reverse('shishiapp_look_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_look(self):
        url = reverse('shishiapp_look_create')
        data = {
            "name": "name",
            "detail": "detail",
            "description": "description",
            "image": "image",
            "created_by": create_django_contrib_auth_models_user().pk,
            "look_has_comment": create_django_contrib_auth_models_user().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_look(self):
        look = create_look()
        url = reverse('shishiapp_look_detail', args=[look.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_look(self):
        look = create_look()
        data = {
            "name": "name",
            "detail": "detail",
            "description": "description",
            "image": "image",
            "created_by": create_django_contrib_auth_models_user().pk,
            "look_has_comment": create_django_contrib_auth_models_user().pk,
        }
        url = reverse('shishiapp_look_update', args=[look.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class CommentOnLookViewTest(unittest.TestCase):
    '''
    Tests for CommentOnLook
    '''
    def setUp(self):
        self.client = Client()

    def test_list_commentonlook(self):
        url = reverse('shishiapp_commentonlook_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_commentonlook(self):
        url = reverse('shishiapp_commentonlook_create')
        data = {
            "name": "name",
            "comment": "comment",
            "stars": "stars",
            "comment_by_user": create_django_contrib_auth_models_user().pk,
            "comment_on_look": create_look().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_commentonlook(self):
        commentonlook = create_commentonlook()
        url = reverse('shishiapp_commentonlook_detail', args=[commentonlook.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_commentonlook(self):
        commentonlook = create_commentonlook()
        data = {
            "name": "name",
            "comment": "comment",
            "stars": "stars",
            "comment_by_user": create_django_contrib_auth_models_user().pk,
            "comment_on_look": create_look().pk,
        }
        url = reverse('shishiapp_commentonlook_update', args=[commentonlook.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


